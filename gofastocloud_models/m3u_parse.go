package gofastocloud_models

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media/m3u"
	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

func ParsePlaylistToPackage(data string) (*player.PackageInfo, error) {
	validSerialDot, _ := regexp.Compile(`[.\s]*S(\d+)[.\s]*E(\d+)\z`)
	stableTitle, _ := regexp.Compile(`(.*) S(\d+) (.*)`)

	streams := []player.ChannelInfo{}
	vods := []player.VodInfo{}
	episodes := []player.VodInfo{}

	seasons := make(map[string]*player.SeasonInfo)
	serials := make(map[string]*player.SerialInfo)

	playlist, err := m3u.ParseToPlaylist(data)
	if err != nil {
		return nil, err
	}

	for _, track := range playlist.Tracks {
		istream := front.IStreamFront{}

		tvgid := track.GetTVGID()
		if tvgid != nil {
			istream.TVGID = *tvgid
		}
		tvgname := track.GetTVGName()
		if tvgname != nil {
			istream.TVGName = *tvgname
		}
		logo := track.GetTVGLogo()
		if logo != nil {
			reflogo := *logo
			if strings.HasPrefix(reflogo, "http") {
				istream.TVGLogo = reflogo
			}
		}
		group := track.GetGroup()
		istream.Name = track.Name

		if istream.TVGName != "" && istream.TVGName != istream.Name {
			istream.Name = istream.TVGName
		}

		out := media.OutputUri{OutputUrl: media.OutputUrl{Uri: track.URI}}
		istream.Output = []media.OutputUri{out}
		if group != nil {
			istream.Groups = []string{*group}
		} else {
			istream.Groups = []string{}
		}

		splited := strings.Split(track.URI, ".")
		ext := strings.ToLower(splited[len(splited)-1])
		if ext == "mp4" || ext == "mkv" || ext == "ts" || ext == "avi" {
			// Juego de tronos S01 E01
			// Last Man Standing S03E15
			// Walking Dead S10 The Walking Dead.S10.E10
			byteName := []byte(istream.Name)
			m := validSerialDot.FindSubmatch(byteName)
			if m != nil {
				serialTitle := byteName[:len(byteName)-len(m[0])]
				seasonNumber := m[1]

				m2 := stableTitle.FindSubmatch(serialTitle)
				if m2 != nil {
					serialTitle = m2[1]
				}

				eid := NewUniqueID()
				epi := *createEpisode(istream, eid)
				episodes = append(episodes, epi)

				ind, _ := strconv.Atoi(string(seasonNumber))
				sid := NewUniqueID()
				if len(istream.Groups) > 0 {
					gr := istream.Groups[0]
					sid += "+G" + gr
					postfix := []byte(" (" + gr + ")")
					serialTitle = append(serialTitle, postfix...)
				}

				seasonName := fmt.Sprintf("S%s %s", seasonNumber, serialTitle)
				serialName := string(serialTitle)

				cur := seasons[seasonName]
				if cur != nil {
					cur.Episodes = append(seasons[seasonName].Episodes, eid)
				} else {
					seasons[seasonName] = createSeason(seasonName, sid, epi.VOD.PreviewIcon, ind, []string{eid})

					ser := serials[serialName]
					if ser != nil {
						ser.Seasons = append(ser.Seasons, seasons[seasonName].ID)
					} else {
						ssid := NewUniqueID()
						ser = createSerial(serialName, ssid, epi.VOD.PreviewIcon, []string{seasons[seasonName].ID}, istream.Groups)
						serials[serialName] = ser
					}
				}
			} else {
				vods = append(vods, *createVod(istream))
			}
		} else {
			streams = append(streams, createLiveStream(istream))
		}
	}

	newSeasons := []player.SeasonInfo{}
	for _, s := range seasons {
		newSeasons = append(newSeasons, *s)
	}

	newSerials := []player.SerialInfo{}
	for _, s := range serials {
		newSerials = append(newSerials, *s)
	}

	back := "https://api.fastocloud.com/install/assets/unknown_background.png"
	cur := gofastogt.MakeUTCTimestamp()
	pub := player.PackagePublic{ID: nil,
		Name:          "Playlist",
		Description:   "Parsed playlist",
		BackgroundURL: back}
	ott := player.OttPackageInfo{
		PackagePublic: pub,

		Streams: streams,
		Vods:    vods,
		Serials: newSerials,

		StreamsVersion: cur,
		VodsVersion:    cur,
		SerialsVersion: cur,

		CreatedDate: cur,
	}

	return &player.PackageInfo{
		OttPackageInfo: ott,

		Episodes: episodes,
		Seasons:  newSeasons,
		Catchups: []player.CatchupInfo{},

		EpisodesVersion: cur,
		SeasonsVersion:  cur,
		CatchupsVersion: cur,
	}, nil
}

func createEpisode(element front.IStreamFront, eid string) *player.VodInfo {
	icon := element.TVGLogo
	if element.TVGLogo == "" {
		icon = "https://api.fastocloud.com/install/assets/unknown_channel.png"
	}

	play := []player.PlayingUrl{}
	for _, uri := range element.Output {
		play = append(play, player.PlayingUrl{Url: uri.OutputUrl.Uri})
	}

	movie := player.MovieInfo{
		Urls:          play,
		Description:   "",
		DisplayName:   element.Name,
		BackgroundURL: icon,
		PreviewIcon:   icon,
		TrailerUrl:    "",
		UserScore:     0.0,
		PrimeDate:     0,
		Country:       "",
		Genres:        []string{},
		Directors:     []string{},
		Cast:          []string{},
		Production:    []string{},
		Duration:      0,
	}

	date := gofastogt.UtcTimeMsec(0)
	base := player.StreamBaseInfo{
		ID:               eid,
		Groups:           element.Groups,
		IARC:             21,
		Parts:            []string{},
		Meta:             []front.MetaURL{},
		Substitles:       []front.Subtitle{},
		Favorite:         false,
		Recent:           0,
		InterruptionTime: 0,
		CreatedDate:      date,
	}

	return &player.VodInfo{
		StreamBaseInfo: base,
		VOD:            movie,
	}
}

func createSeason(seasonName string, sid string, previewIcon string, ind int, eid []string) *player.SeasonInfo {
	back := "https://api.fastocloud.com/install/assets/unknown_background.png"

	return &player.SeasonInfo{
		ID:          sid,
		Name:        seasonName,
		Background:  back,
		Icon:        previewIcon,
		Description: "",
		Season:      ind,
		Episodes:    eid,
		CreatedDate: 0,
	}
}

func createSerial(title string, sid string, icon string, seasons, groups []string) *player.SerialInfo {
	back := "https://api.fastocloud.com/install/assets/unknown_background.png"

	return &player.SerialInfo{
		ID:          sid,
		Name:        title,
		Background:  back,
		Icon:        icon,
		Groups:      groups,
		IARC:        21,
		Description: "",
		PrimeDate:   0,
		Seasons:     seasons,
		CreatedDate: 0,
		UserScore:   0.0,
		Country:     "",
		Genres:      []string{},
		Directors:   []string{},
		Cast:        []string{},
		Production:  []string{},
	}
}

func createVod(element front.IStreamFront) *player.VodInfo {
	icon := element.TVGLogo
	if element.TVGLogo == "" {
		icon = "https://api.fastocloud.com/install/assets/unknown_channel.png"
	}

	play := []player.PlayingUrl{}
	for _, uri := range element.Output {
		play = append(play, player.PlayingUrl{Url: uri.OutputUrl.Uri})
	}

	movie := player.MovieInfo{
		Urls:          play,
		Description:   "",
		DisplayName:   element.Name,
		BackgroundURL: icon,
		PreviewIcon:   icon,
		TrailerUrl:    "",
		UserScore:     0.0,
		PrimeDate:     0,
		Country:       "",
		Genres:        []string{},
		Directors:     []string{},
		Cast:          []string{},
		Production:    []string{},
		Duration:      0,
	}

	sid := NewUniqueID()
	date := gofastogt.UtcTimeMsec(0)
	base := player.StreamBaseInfo{
		ID:               sid,
		Groups:           element.Groups,
		IARC:             21,
		Parts:            []string{},
		Meta:             []front.MetaURL{},
		Substitles:       []front.Subtitle{},
		Favorite:         false,
		Recent:           0,
		InterruptionTime: 0,
		CreatedDate:      date,
	}

	return &player.VodInfo{
		StreamBaseInfo: base,
		VOD:            movie,
	}
}

func createLiveStream(element front.IStreamFront) player.ChannelInfo {
	eid := element.TVGID

	icon := element.TVGLogo
	if element.TVGLogo == "" {
		icon = "https://api.fastocloud.com/install/assets/unknown_channel.png"
	}

	play := []player.PlayingUrl{}
	for _, uri := range element.Output {
		play = append(play, player.PlayingUrl{Url: uri.OutputUrl.Uri})
	}
	epg := player.EpgInfo{
		ID:          eid,
		Urls:        play,
		DisplayName: element.Name,
		Icon:        icon,
		Description: "",
	}
	sid := NewUniqueID()
	date := gofastogt.UtcTimeMsec(0)
	return player.ChannelInfo{
		StreamBaseInfo: player.StreamBaseInfo{
			ID:               sid,
			Groups:           element.Groups,
			IARC:             21,
			Parts:            []string{},
			Favorite:         false,
			Recent:           0,
			InterruptionTime: 0,
			Meta:             []front.MetaURL{},
			Substitles:       []front.Subtitle{},
			CreatedDate:      date,
		},
		EpgInfo: epg,
	}
}
