package gofastocloud_models

type IntegrationService int

const (
	DUMMY IntegrationService = iota
	STRIPE
	PAYPAL
	WOOCOMMERCE
)

type IntegrationLink struct {
	Type IntegrationService `bson:"type"`
	IID  string             `bson:"iid"`
}
