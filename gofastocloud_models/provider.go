package gofastocloud_models

import (
	"net/mail"
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DBProviderFields struct {
	Email     string               `bson:"email"`
	FirstName string               `bson:"first_name"`
	LastName  string               `bson:"last_name"`
	Password  string               `bson:"password"`
	Type      front.ProviderType   `bson:"type"`
	Status    front.ProviderStatus `bson:"status"`
	Country   string               `bson:"country"`
	Language  string               `bson:"language"`
	Credits   uint64               `bson:"credits"`

	// filled by server
	CreatedDate *time.Time           `bson:"created_date,omitempty"`
	Subscribers []primitive.ObjectID `bson:"subscribers"`
	Providers   []primitive.ObjectID `bson:"providers"`
	CheckPoints []DBCheckPoint       `bson:"checkpoints"`
}

type DBProvider struct {
	ID               primitive.ObjectID `bson:"_id"`
	DBProviderFields `bson:",inline"`
}

func (p *DBProviderFields) IsAdmin() bool {
	return p.Type == front.ADMIN
}

func (p *DBProviderFields) IsReseller() bool {
	return p.Type == front.RESELLER
}

func (p *DBProviderFields) IsSuper() bool {
	return p.Type == front.SUPER_RESELLER
}

func (p *DBProviderFields) IsChannelAdmin() bool {
	return p.Type == front.CHANNEL_ADMIN
}

func (p *DBProviderFields) IsAnalytics() bool {
	return p.Type == front.ANALYTICS
}

func (p *DBProviderFields) ContainsSubscriber(sid primitive.ObjectID) bool {
	for _, sub := range p.Subscribers {
		if sub == sid {
			return true
		}
	}

	return false
}

func (p *DBProviderFields) ContainsProvider(sid primitive.ObjectID) bool {
	for _, prov := range p.Providers {
		if prov == sid {
			return true
		}
	}

	return false
}

func (p *DBProviderFields) IsValidEmail() bool {
	_, err := mail.ParseAddress(p.Email)
	return err == nil
}

func (p *DBProvider) ToFront(owner *string, creditsRemaining uint64) *front.ProviderFront {
	hexed := p.ID.Hex()
	var created *gofastogt.UtcTimeMsec
	if p.CreatedDate != nil {
		tmp := gofastogt.Time2UtcTimeMsec(*p.CreatedDate)
		created = &tmp
	}

	up := front.ProviderSignUpFront{
		Email:     p.Email,
		FirstName: p.FirstName,
		LastName:  p.LastName,
		Password:  p.Password,
		Country:   p.Country,
		Language:  p.Language,
		Credits:   p.Credits,
		Type:      p.Type,
		Status:    p.Status,
	}

	add := front.ProviderAddFront{
		ProviderSignUpFront: up,
		CreatedDate:         created,
		Owner:               owner,
	}

	return &front.ProviderFront{ID: &hexed,
		ProviderAddFront: add,
		CreditsRemaining: &creditsRemaining,
	}
}

func MakeProviderFromAddFront(p *front.ProviderAddFront) *DBProviderFields {
	var created *time.Time
	if p.CreatedDate != nil {
		tmp := gofastogt.UtcTime2Time(*p.CreatedDate)
		created = &tmp
	}

	return &DBProviderFields{
		Email:       p.Email,
		FirstName:   p.FirstName,
		LastName:    p.LastName,
		Language:    p.Language,
		Country:     p.Country,
		Password:    p.Password,
		Type:        p.Type,
		Status:      p.Status,
		Credits:     p.Credits,
		CreatedDate: created,
	}
}
