package crocott

import "gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"

type Info struct {
	ProvidersCount int64                      `json:"providers_count"`
	Brand          PublicBrand                `json:"brand"`
	ExpTime        int                        `json:"exp"`
	Packages       []front.PackagePublicFront `json:"packages"`
	Version        int64                      `json:"version"`
}
