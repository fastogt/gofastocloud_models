package crocott

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type PublicBrand struct {
	Title        string            `yaml:"title" json:"title"`
	Logo         string            `yaml:"logo" json:"logo"`
	Landing      string            `yaml:"landing" json:"landing"`
	Signin       string            `yaml:"signin" json:"signin"`
	Signup       string            `yaml:"signup" json:"signup"`
	Intro        string            `yaml:"intro" json:"intro"`
	Organization string            `yaml:"organization" json:"organization"`
	Contact      string            `yaml:"contact" json:"contact"`
	Theme        ThemeType         `yaml:"theme" json:"theme"`
	Mode         WsMode            `yaml:"mode" json:"mode"`
	Platforms    []PlayersPlatform `yaml:"platforms,omitempty" json:"platforms,omitempty"`
}

func (p *PublicBrand) UnmarshalJSON(data []byte) error {
	request := struct {
		Title        *string           `json:"title"`
		Logo         *string           `json:"logo"`
		Landing      *string           `json:"landing"`
		Signin       *string           `json:"signin"`
		Signup       *string           `json:"signup"`
		Intro        *string           `json:"intro"`
		Organization *string           `json:"organization"`
		Contact      *string           `json:"contact"`
		Theme        *ThemeType        `json:"theme"`
		Mode         *WsMode           `json:"mode"`
		Platforms    []PlayersPlatform `json:"platforms,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Title == nil {
		return errors.New("title field is required")
	}
	if request.Logo == nil {
		return errors.New("logo field is required")
	}
	if request.Landing == nil {
		return errors.New("landing field is required")
	}
	if request.Signin == nil {
		return errors.New("signin field is required")
	}
	if request.Signup == nil {
		return errors.New("signup field is required")
	}
	if request.Intro == nil {
		return errors.New("intro field is required")
	}
	if request.Organization == nil {
		return errors.New("organization field is required")
	}
	if request.Contact == nil {
		return errors.New("contact field is required")
	}
	if request.Theme == nil {
		return errors.New("theme field is required")
	}
	if request.Mode == nil {
		return errors.New("mode field is required")
	}
	p.Title = *request.Title
	p.Logo = *request.Logo
	p.Landing = *request.Landing
	p.Signin = *request.Signin
	p.Signup = *request.Signup
	p.Intro = *request.Intro
	p.Organization = *request.Organization
	p.Contact = *request.Contact
	p.Theme = *request.Theme
	p.Mode = *request.Mode
	p.Platforms = request.Platforms
	return nil
}

type ThemeType int

const (
	WHITE ThemeType = iota
	BLACK
)

type WsMode int

const (
	OTT WsMode = iota
	IPTV
	CCTV
	DRONE
)

type PlayersPlatform struct {
	Os  gofastogt.PlatformType `yaml:"os" json:"os"`
	Url string                 `yaml:"url" json:"url"`
}

func (p *PlayersPlatform) UnmarshalJSON(data []byte) error {
	request := struct {
		Os  *string `json:"os"`
		Url *string `json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}

	if request.Os == nil {
		return errors.New("name field required")
	}

	if len(*request.Os) == 0 {
		return errors.New("invalid name")
	}

	p.Os = gofastogt.MakePlatformTypeFromString(*request.Os)
	p.Url = *request.Url
	return nil
}
