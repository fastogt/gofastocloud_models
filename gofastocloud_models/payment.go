package gofastocloud_models

import "gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"

type PaymentStripeData struct {
	PubKey         string `yaml:"pub_key"`
	SecretKey      string `yaml:"secret_key"`
	EndpointSecret string `yaml:"endpoint_secret"`
	WebhookId      string `yaml:"webhook_id"`
}

func (pd *PaymentStripeData) ToFront() *front.PaymentStripeDataFront {
	return &front.PaymentStripeDataFront{
		PubKey:    pd.PubKey,
		SecretKey: pd.SecretKey,
	}

}

type PaymentPaypalData struct {
	ClientId     string           `yaml:"client_id"`
	ClientSecret string           `yaml:"client_secret"`
	Mode         front.PaypalMode `yaml:"mode"`
	WebhookId    string           `yaml:"webhook_id"`
}

func (pd *PaymentPaypalData) ToFront() *front.PaymentPaypalDataFront {
	return &front.PaymentPaypalDataFront{
		ClientId:     pd.ClientId,
		ClientSecret: pd.ClientSecret,
		Mode:         pd.Mode,
	}
}

type Payment struct {
	Type front.PaymentType `yaml:"type"`

	Stripe *PaymentStripeData `yaml:"stripe,omitempty"`
	Paypal *PaymentPaypalData `yaml:"paypal,omitempty"`
}

func (pd *Payment) ToFront() *front.PaymentFront {
	var paypal *front.PaymentPaypalDataFront
	if pd.Paypal != nil {
		paypal = pd.Paypal.ToFront()
	}
	var stripe *front.PaymentStripeDataFront
	if pd.Stripe != nil {
		stripe = pd.Stripe.ToFront()
	}
	return &front.PaymentFront{
		Type:   pd.Type,
		Paypal: paypal,
		Stripe: stripe,
	}
}
