package front

import (
	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type SerialFront struct{ front.SerialFront }

func (s *SerialFront) ToPlayer(pid *string) player.SerialInfo {
	var id string
	if s.ID != nil {
		id = *s.ID
	} else {
		id = ""
	}

	var createDate gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		createDate = *s.CreatedDate
	} else {
		createDate = 0
	}

	return player.SerialInfo{
		ID:          id,
		Name:        s.Name,
		Background:  s.Background,
		Icon:        s.Icon,
		IARC:        s.IARC,
		Groups:      s.Groups,
		Description: s.Description,
		PrimeDate:   s.PrimeDate,
		Seasons:     s.Seasons,
		CreatedDate: createDate,
		UserScore:   s.UserScore,
		Country:     s.Country,
		Genres:      s.Genres,
		Directors:   s.Directors,
		Cast:        s.Cast,
		Production:  s.Production,

		Price:     s.Price,
		PackageID: pid,
	}
}
