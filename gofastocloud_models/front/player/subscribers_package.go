package player

import (
	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type PackagePublic struct {
	ID            *string          `json:"id,omitempty"`
	Name          string           `json:"name"`
	Price         *front.PricePack `json:"price,omitempty"`
	Description   string           `json:"description"`
	BackgroundURL string           `json:"background_url"`
}

type OttPackageInfo struct {
	PackagePublic

	Streams []ChannelInfo `json:"streams"`
	Vods    []VodInfo     `json:"vods"`
	Serials []SerialInfo  `json:"serials"`

	// versions
	StreamsVersion gofastogt.UtcTimeMsec `json:"streams_version"`
	VodsVersion    gofastogt.UtcTimeMsec `json:"vods_version"`
	SerialsVersion gofastogt.UtcTimeMsec `json:"serials_version"`

	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`
}

type PackageInfo struct {
	OttPackageInfo

	Episodes []VodInfo     `json:"episodes"`
	Seasons  []SeasonInfo  `json:"seasons"`
	Catchups []CatchupInfo `json:"catchups"`

	// versions
	EpisodesVersion gofastogt.UtcTimeMsec `json:"episodes_version"`
	SeasonsVersion  gofastogt.UtcTimeMsec `json:"seasons_version"`
	CatchupsVersion gofastogt.UtcTimeMsec `json:"catchups_version"`
}
