package player

import (
	"gitlab.com/fastocloud/gofastocloud/gofastocloud/media"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type PlayingUrlType int

const (
	UNKNOWN PlayingUrlType = iota
	WHEP
	YOUTUBE
	EMBED
)

type PlayingUrl struct {
	Url  string         `json:"url"`
	Type PlayingUrlType `json:"type"`
}

func MakePlayingUrlFromOutputUri(out media.OutputUri) PlayingUrl {
	play := PlayingUrl{Url: out.Uri, Type: UNKNOWN}
	if out.Whip != nil {
		play.Type = WHEP
	}
	return play
}

type Comment struct {
	Text      string                `json:"text"`
	PrimeDate gofastogt.UtcTimeMsec `json:"created_date"`
}
