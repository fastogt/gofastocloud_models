package player

import (
	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

//Life Stream models for subscriber devices

type StreamBaseInfo struct {
	ID          string                `json:"id"`
	Groups      []string              `json:"groups"`
	IARC        int                   `json:"iarc"`
	Parts       []string              `json:"parts"`
	Meta        []front.MetaURL       `json:"meta"`
	Substitles  []front.Subtitle      `json:"subtitles"`
	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`

	// optional
	Price     *front.PricePack `json:"price,omitempty"`
	PackageID *string          `json:"pid,omitempty"`

	// user
	Favorite         bool                   `json:"favorite"`
	Recent           gofastogt.UtcTimeMsec  `json:"recent"`
	InterruptionTime gofastogt.DurationMsec `json:"interrupt_time"`
}

type NameIcon struct {
	ID          string `json:"id"`
	DisplayName string `json:"name"`
	Icon        string `json:"icon"`
}

type EpgInfo struct {
	ID string `json:"id"` // epg id, tvg_id

	Urls        []PlayingUrl `json:"urls"`
	DisplayName string       `json:"display_name"`
	Icon        string       `json:"icon"`
	Description string       `json:"description"`
}

type ChannelInfo struct {
	StreamBaseInfo
	EpgInfo EpgInfo `json:"epg"`
}

func (s *ChannelInfo) ToNameIcon() NameIcon {
	return NameIcon{ID: s.ID, DisplayName: s.EpgInfo.DisplayName, Icon: s.EpgInfo.Icon}
}

// VODS Stream models for subscriber devices
type MovieInfo struct {
	Urls        []PlayingUrl `json:"urls"`
	DisplayName string       `json:"display_name"`
	Description string       `json:"description"`
	PreviewIcon string       `json:"icon"`

	BackgroundURL string                 `json:"background_url"`
	TrailerUrl    string                 `json:"trailer_url"`
	UserScore     float64                `json:"user_score"`
	PrimeDate     gofastogt.UtcTimeMsec  `json:"prime_date"`
	Country       string                 `json:"country"`
	Genres        []string               `json:"genres"`
	Directors     []string               `json:"directors"`
	Cast          []string               `json:"cast"`
	Production    []string               `json:"production"`
	Duration      gofastogt.DurationMsec `json:"duration"`
}

type VodInfo struct {
	StreamBaseInfo
	VOD MovieInfo `json:"vod"`
}

func (s *VodInfo) ToNameIcon() NameIcon {
	return NameIcon{ID: s.ID, DisplayName: s.VOD.DisplayName, Icon: s.VOD.PreviewIcon}
}

// Catchup Stream models for subscriber devices
type CatchupInfo struct {
	ChannelInfo
	StartRecord gofastogt.UtcTimeMsec `json:"start"`
	StopRecord  gofastogt.UtcTimeMsec `json:"stop"`
}

type SeasonInfo struct {
	ID          string                `json:"id"`
	Name        string                `json:"name"`
	Background  string                `json:"background_url"`
	Icon        string                `json:"icon"`
	Description string                `json:"description"`
	Season      int                   `json:"season"`
	Episodes    []string              `json:"episodes"`
	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`

	// optional
	PackageID *string `json:"pid,omitempty"`
}

func (s *SeasonInfo) ContainsEpisode(eid string) bool {
	for _, e := range s.Episodes {
		if e == eid {
			return true
		}
	}
	return false
}

func (s *SeasonInfo) ToNameIcon() NameIcon {
	return NameIcon{ID: s.ID, DisplayName: s.Name, Icon: s.Icon}
}

type SerialInfo struct {
	ID          string                `json:"id"`
	Name        string                `json:"name"`
	Background  string                `json:"background_url"`
	Icon        string                `json:"icon"`
	IARC        int                   `json:"iarc"`
	Groups      []string              `json:"groups"`
	Description string                `json:"description"`
	PrimeDate   gofastogt.UtcTimeMsec `json:"prime_date"`
	Seasons     []string              `json:"seasons"`
	CreatedDate gofastogt.UtcTimeMsec `json:"created_date"`

	// new fields
	UserScore  float64  `json:"user_score"`
	Country    string   `json:"country"`
	Genres     []string `json:"genres"`
	Directors  []string `json:"directors"`
	Cast       []string `json:"cast"`
	Production []string `json:"production"`

	// optional
	Price     *front.PricePack `json:"price,omitempty"`
	PackageID *string          `json:"pid,omitempty"`

	// user
	Favorite bool                  `json:"favorite"`
	Recent   gofastogt.UtcTimeMsec `json:"recent"`
}

func (s *SerialInfo) ContainsSeason(sid string) bool {
	for _, s := range s.Seasons {
		if s == sid {
			return true
		}
	}
	return false
}

func (s *SerialInfo) ToNameIcon() NameIcon {
	return NameIcon{ID: s.ID, DisplayName: s.Name, Icon: s.Icon}
}
