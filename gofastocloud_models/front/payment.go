package front

import (
	"encoding/json"
	"errors"
)

type PaymentType int

func (p PaymentType) IsValid() bool {
	return p == STRIPE || p == PAYPAL
}

const (
	STRIPE PaymentType = iota
	PAYPAL
)

type PaymentFront struct {
	Type PaymentType `json:"type"`

	Stripe *PaymentStripeDataFront `json:"stripe,omitempty"`
	Paypal *PaymentPaypalDataFront `json:"paypal,omitempty"`
}

func (p *PaymentFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Type *PaymentType `json:"type"`

		Stripe *PaymentStripeDataFront `json:"stripe,omitempty"`
		Paypal *PaymentPaypalDataFront `json:"paypal,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Type == nil {
		return errors.New("type field is required")
	}
	if !request.Type.IsValid() {
		return errors.New("invalid input paymentType")
	}

	if *request.Type == STRIPE && request.Stripe != nil {
		p.Type = *request.Type
		p.Stripe = request.Stripe
		return nil
	} else if *request.Type == PAYPAL && request.Paypal != nil {
		p.Type = *request.Type
		p.Paypal = request.Paypal
		return nil
	}
	return errors.New("invalid input Payment")
}

type PaymentStripeDataFront struct {
	PubKey    string `json:"pub_key"`
	SecretKey string `json:"secret_key"`
}

type PaypalMode int

const (
	SANDBOX PaypalMode = iota
	LIVE
)

type PaymentPaypalDataFront struct {
	ClientId     string     `json:"client_id"`
	ClientSecret string     `json:"client_secret"`
	Mode         PaypalMode `json:"mode"`
}
