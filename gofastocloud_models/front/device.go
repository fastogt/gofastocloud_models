package front

import (
	"encoding/json"
	"errors"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type DeviceStatus int

const (
	NOT_ACTIVE_DEVICE DeviceStatus = iota
	ACTIVE_DEVICE
	BANNED_DEVICE
)

type CPUBrand string

type DeviceOS struct { // bson needed using in subscriber.devices
	Name     gofastogt.PlatformType `bson:"name" json:"name"`
	Version  string                 `bson:"version" json:"version"`
	Arch     string                 `bson:"arch" json:"arch"`
	RamTotal int64                  `bson:"ram_total" json:"ram_total"`
	RamFree  int64                  `bson:"ram_free" json:"ram_free"`
}

func (os *DeviceOS) UnmarshalJSON(data []byte) error {
	request := struct {
		Name     *string `json:"name"`
		Version  *string `json:"version"`
		Arch     *string `json:"arch"`
		RamTotal *int64  `json:"ram_total"`
		RamFree  *int64  `json:"ram_free"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("os Name can't be empty")
	}

	if len(*request.Name) == 0 {
		return errors.New("invalid os name")
	}

	if request.Version == nil {
		return errors.New("os Version can't be empty")
	}
	if request.Arch == nil {
		return errors.New("os Arch can't be empty")
	}
	if request.RamTotal == nil {
		return errors.New("os RamTotal can't be empty")
	}
	if request.RamFree == nil {
		return errors.New("os RamFree can't be empty")
	}

	os.Name = gofastogt.MakePlatformTypeFromString(*request.Name)
	os.Version = *request.Version
	os.Arch = *request.Arch
	os.RamFree = *request.RamFree
	os.RamTotal = *request.RamTotal
	return nil
}

type DeviceProject struct { // bson needed using in device
	Name    string `bson:"name" json:"name"`
	Version string `bson:"version" json:"version"`
}

func (d *DeviceProject) UnmarshalJSON(data []byte) error {
	request := struct {
		Name    *string `json:"name"`
		Version *string `json:"version"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Name == nil {
		return errors.New("device project name can't be empty")
	}
	if request.Version == nil {
		return errors.New("device project version can't be empty")
	}
	d.Name = *request.Name
	d.Version = *request.Version
	return nil
}

type DevicesFront struct {
	IDDevice    *string                `json:"id,omitempty"`
	Name        string                 `json:"name"`
	Status      DeviceStatus           `json:"status"`
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
}

type DeviceLoginFront struct {
	DevicesFront
	OS             *DeviceOS               `json:"os,omitempty"`
	Project        *DeviceProject          `json:"project,omitempty"`
	CPU            *CPUBrand               `json:"cpu_brand,omitempty"`
	Logins         []DeviceLoginInfoFront  `json:"logins,omitempty"`
	ActiveDuration *gofastogt.DurationMsec `json:"active_duration,omitempty"`
	LastActiveTime *gofastogt.UtcTimeMsec  `json:"last_active,omitempty"`
}

type DeviceLoginInfoFront struct {
	IP        string                `json:"ip"`
	Timestamp gofastogt.UtcTimeMsec `json:"timestamp"`
	Location  string                `json:"loc,omitempty"`
	City      string                `json:"city,omitempty"`
	Country   string                `json:"country,omitempty"`
}

type SubscriberDevice struct {
	IDSubscriber string                 `bson:"sid"         json:"sid"`
	DeviceID     string                 `bson:"device_id"   json:"device_id"`
	DeviceName   string                 `bson:"device_name" json:"device_name"`
	DeviceOS     gofastogt.PlatformType `bson:"device_os"   json:"device_os"`
}

type DeviceStatFront struct { // bson needed using in snapshots
	SubscriberDevice `bson:",inline"`

	ActiveDuration  *gofastogt.DurationMsec `bson:"active_duration,omitempty"  json:"active_duration,omitempty"`
	LoginsCount     int64                   `bson:"logins_count"               json:"logins_count"`
	ActivationPlace *DeviceLoginInfoFront   `bson:"activation_place,omitempty" json:"activation_place,omitempty"`
	LastPlace       *DeviceLoginInfoFront   `bson:"last_place,omitempty" json:"last_place,omitempty"`
}
