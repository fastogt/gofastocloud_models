package front

import (
	"encoding/json"
	"errors"
	"net/mail"

	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type ProviderType int

type ProviderStatus int

const (
	NO_ACTIVE ProviderStatus = iota
	ACTIVE
	BANNED
)

const (
	ADMIN ProviderType = iota
	SUPER_RESELLER
	RESELLER
	CHANNEL_ADMIN
	ANALYTICS
)

type ProviderSignUpFront struct {
	Email     string         `json:"email"`
	FirstName string         `json:"first_name"`
	LastName  string         `json:"last_name"`
	Password  string         `json:"password"`
	Country   string         `json:"country"`
	Language  string         `json:"language"`
	Credits   uint64         `json:"credits"`
	Type      ProviderType   `json:"type"`
	Status    ProviderStatus `json:"status"`
}

func (s *ProviderSignUpFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Email       *string                `json:"email"`
		FirstName   *string                `json:"first_name"`
		LastName    *string                `json:"last_name"`
		Password    *string                `json:"password"`
		Country     *string                `json:"country"`
		Language    *string                `json:"language"`
		Credits     *uint64                `json:"credits"`
		Type        *ProviderType          `json:"type"`
		Status      *ProviderStatus        `json:"status"`
		CreatedDate *gofastogt.UtcTimeMsec `json:"created_date"`
		Owner       *string                `json:"owner,omitempty"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password field required")
	}
	if len(*request.Password) == 0 {
		return errors.New("password can't be empty")
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.Type == nil {
		return errors.New("type can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.Credits == nil {
		return errors.New("credits can't be empty")
	}
	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = *request.Password
	s.Country = *request.Country
	s.Language = *request.Language
	s.Type = *request.Type
	s.Status = *request.Status
	s.Credits = *request.Credits

	return nil
}

type ProviderAddFront struct {
	ProviderSignUpFront
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Owner       *string                `json:"owner,omitempty"`
}

func (s *ProviderAddFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Email     *string         `json:"email"`
		FirstName *string         `json:"first_name"`
		LastName  *string         `json:"last_name"`
		Password  *string         `json:"password"`
		Country   *string         `json:"country"`
		Language  *string         `json:"language"`
		Credits   *uint64         `json:"credits"`
		Type      *ProviderType   `json:"type"`
		Status    *ProviderStatus `json:"status"`

		CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
		Owner       *string                `json:"owner,omitempty"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password field required")
	}
	if len(*request.Password) == 0 {
		return errors.New("password can't be empty")
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.Type == nil {
		return errors.New("type can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.Credits == nil {
		return errors.New("credits can't be empty")
	}

	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = *request.Password
	s.Country = *request.Country
	s.Language = *request.Language
	s.Type = *request.Type
	s.Status = *request.Status
	s.Credits = *request.Credits
	s.CreatedDate = request.CreatedDate
	s.Owner = request.Owner
	return nil
}

type ProviderFront struct {
	ProviderAddFront
	ID               *string `json:"id,omitempty"`
	CreditsRemaining *uint64 `json:"credits_remaining,omitempty"`
}

// CreaditsRemanin don't post from font.
func (p *ProviderFront) UnmarshalJSON(data []byte) error {
	request := struct {
		ID               *string                `json:"id,omitempty"`
		Email            *string                `json:"email"`
		FirstName        *string                `json:"first_name"`
		LastName         *string                `json:"last_name"`
		Password         *string                `json:"password,omitempty"`
		CreatedDate      *gofastogt.UtcTimeMsec `json:"created_date"`
		Type             *ProviderType          `json:"type"`
		Status           *ProviderStatus        `json:"status"`
		Country          *string                `json:"country"`
		Language         *string                `json:"language"`
		Credits          *uint64                `json:"credits"`
		CreditsRemaining *uint64                `json:"credits_remaining"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password field required")
	}
	/*if len(*request.Password) == 0 {
		return errors.New("password can't be empty")
	}*/
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Type == nil {
		return errors.New("type can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.Credits == nil {
		return errors.New("credits can't be empty")
	}

	p.ID = request.ID
	p.Email = *request.Email
	p.FirstName = *request.FirstName
	p.LastName = *request.LastName
	p.Password = *request.Password
	p.CreatedDate = request.CreatedDate
	p.Type = *request.Type
	p.Status = *request.Status
	p.Country = *request.Country
	p.Language = *request.Language
	p.Credits = *request.Credits
	p.CreditsRemaining = request.CreditsRemaining
	return nil
}
