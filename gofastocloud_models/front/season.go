package front

import (
	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type SeasonFront struct{ front.SeasonFront }

func (s *SeasonFront) ToPlayer(pid *string) player.SeasonInfo {
	var id string
	if s.ID != nil {
		id = *s.ID
	} else {
		id = "" // #FIXME: never should happend
	}

	var createDate gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		createDate = *s.CreatedDate
	} else {
		createDate = 0
	}

	return player.SeasonInfo{
		ID:          id,
		Name:        s.Name,
		Background:  s.Background,
		Icon:        s.Icon,
		Description: s.Description,
		Season:      s.Season,
		Episodes:    s.Episodes,
		CreatedDate: createDate,
		PackageID:   pid,
	}
}
