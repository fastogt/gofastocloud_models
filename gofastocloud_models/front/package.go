package front

import (
	"encoding/json"

	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type PackagePublicFront struct {
	player.PackagePublic

	Location *gofastogt.Location `json:"location,omitempty"`
	//
	CreatedDate *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	Version     *string                `json:"version,omitempty"`
}

func (p *PackagePublicFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(p)
}

func (pack *PackagePublicFront) IsPaid() bool {
	return pack.Price != nil
}

type PackageFront struct {
	PackagePublicFront
	Local             bool                `json:"local"`
	Host              front.WSServerFront `json:"host"`
	Visible           bool                `json:"visible"`
	AvailableOnSignup bool                `json:"available_on_signup"`
}

func (p *PackageFront) ToBytes() (json.RawMessage, error) {
	return json.Marshal(p)
}

func (p *PackageFront) ToPublic() PackagePublicFront {
	pub := p.PackagePublicFront
	return pub
}
