package front

import (
	"encoding/json"
	"errors"
	"net/mail"

	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type StatusSubscriber int

const (
	NOT_ACTIVE_SUB StatusSubscriber = iota
	ACTIVE_SUB
	DELETED_SUB
)

type UserPlaylistFront struct {
	IDPlaylist *string `json:"id,omitempty"`
	Name       string  `json:"name"`
	Url        string  `json:"url"`
	Selected   bool    `json:"select"`
}

type SubscriberSignUpFront struct {
	FirstName string  `json:"first_name"`
	LastName  string  `json:"last_name"`
	Email     string  `json:"email"`
	Password  string  `json:"password"`
	Country   string  `json:"country"`
	Language  string  `json:"language"`
	Phone     *string `json:"phone,omitempty"`
	Details   *string `json:"details,omitempty"`
}

func MakeSubscriberSignUpFront(firstName string, lastName string, email string, password string,
	country string, language string) *SubscriberSignUpFront {
	return &SubscriberSignUpFront{
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Password:  password,
		Country:   country,
		Language:  language,
	}
}

func (s *SubscriberSignUpFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Email     *string `json:"email"`
		FirstName *string `json:"first_name"`
		LastName  *string `json:"last_name"`
		Password  *string `json:"password"`
		Country   *string `json:"country"`
		Language  *string `json:"language"`
		Phone     *string `json:"phone"`
		Details   *string `json:"details"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password field required")
	}
	if len(*request.Password) == 0 {
		return errors.New("password can't be empty")
	}
	if request.Phone != nil {
		if len(*request.Phone) == 0 {
			return errors.New("phone can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = *request.Password
	s.Country = *request.Country
	s.Language = *request.Language
	s.Phone = request.Phone
	s.Details = request.Details

	return nil
}

type SubscriberAddFront struct {
	SubscriberSignUpFront
	CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
	ExpDate        gofastogt.UtcTimeMsec  `json:"exp_date"`
	Status         StatusSubscriber       `json:"status"`
	MaxDeviceCount uint64                 `json:"max_devices_count"`
	Servers        []string               `json:"servers"`
	Owner          *string                `json:"owner,omitempty"`
}

func (s *SubscriberAddFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Email     *string `json:"email"`
		FirstName *string `json:"first_name"`
		LastName  *string `json:"last_name"`
		Password  *string `json:"password"`
		Country   *string `json:"country"`
		Language  *string `json:"language"`
		Phone     *string `json:"phone"`
		Details   *string `json:"details"`

		CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
		ExpDate        *gofastogt.UtcTimeMsec `json:"exp_date"`
		Status         *StatusSubscriber      `json:"status"`
		MaxDeviceCount *uint64                `json:"max_devices_count"`
		Servers        *[]string              `json:"servers"`
		Owner          *string                `json:"owner,omitempty"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password field required")
	}
	if len(*request.Password) == 0 {
		return errors.New("password can't be empty")
	}
	if request.Phone != nil {
		if len(*request.Phone) == 0 {
			return errors.New("phone can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.MaxDeviceCount == nil {
		return errors.New("max_devices_count can't be empty")
	}
	if request.ExpDate == nil {
		return errors.New("exp_date can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.Servers == nil {
		return errors.New("servers can't be empty")
	}
	for _, s := range *request.Servers {
		_, err := primitive.ObjectIDFromHex(s)
		if err != nil {
			return errors.New("servers should be hexed ObjectID")
		}
	}

	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = *request.Password
	s.Country = *request.Country
	s.Language = *request.Language
	s.Phone = request.Phone
	s.Details = request.Details
	s.CreatedDate = request.CreatedDate
	s.ExpDate = *request.ExpDate
	s.Status = *request.Status
	s.MaxDeviceCount = *request.MaxDeviceCount
	s.Servers = *request.Servers
	s.Owner = request.Owner
	return nil
}

type SubscriberFront struct {
	SubscriberAddFront
	ID      *string  `json:"id,omitempty"`
	SID     *string  `json:"sid,omitempty"`
	Devices []string `json:"devices"`
}

func (s *SubscriberFront) UnmarshalJSON(data []byte) error {
	request := struct {
		Id             *string                `json:"id,omitempty"`
		Email          *string                `json:"email"`
		FirstName      *string                `json:"first_name"`
		LastName       *string                `json:"last_name"`
		Password       *string                `json:"password,omitempty"`
		SID            *string                `json:"sid,omitempty"`
		Phone          *string                `json:"phone,omitempty"`
		Details        *string                `json:"details,omitempty"`
		Devices        *[]string              `json:"devices"`
		Country        *string                `json:"country"`
		Language       *string                `json:"language"`
		CreatedDate    *gofastogt.UtcTimeMsec `json:"created_date,omitempty"`
		ExpDate        *gofastogt.UtcTimeMsec `json:"exp_date"`
		Status         *StatusSubscriber      `json:"status"`
		MaxDeviceCount *uint64                `json:"max_devices_count"`
		Servers        *[]string              `json:"servers"`
	}{}

	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	_, err = mail.ParseAddress(*request.Email)
	if err != nil {
		return err
	}
	if request.Password == nil {
		return errors.New("password field required")
	}
	/*if len(*request.Password) == 0 {
		return errors.New("password can't be empty")
	}*/
	if request.SID != nil {
		if len(*request.SID) == 0 {
			return errors.New("sid can't be empty")
		}
	}
	if request.FirstName == nil {
		return errors.New("firstName can't be empty")
	}
	if request.LastName == nil {
		return errors.New("lastName can't be empty")
	}
	if request.Country == nil {
		return errors.New("country can't be empty")
	}
	if request.Language == nil {
		return errors.New("language can't be empty")
	}
	if request.MaxDeviceCount == nil {
		return errors.New("maxDeviceCount can't be empty")
	}
	if request.ExpDate == nil {
		return errors.New("exp_date can't be empty")
	}
	if request.Status == nil {
		return errors.New("status can't be empty")
	}
	if request.Servers == nil {
		return errors.New("servers can't be empty")
	}
	for _, s := range *request.Servers {
		_, err := primitive.ObjectIDFromHex(s)
		if err != nil {
			return errors.New("servers should be hexed ObjectID")
		}
	}
	if request.Devices == nil {
		return errors.New("devices can't be empty")
	}

	s.ID = request.Id
	s.Email = *request.Email
	s.FirstName = *request.FirstName
	s.LastName = *request.LastName
	s.Password = *request.Password
	s.Country = *request.Country
	s.Language = *request.Language
	s.SID = request.SID
	s.Phone = request.Phone
	s.Details = request.Details
	s.CreatedDate = request.CreatedDate
	s.ExpDate = *request.ExpDate
	s.Status = *request.Status
	s.MaxDeviceCount = *request.MaxDeviceCount
	s.Servers = *request.Servers
	s.Devices = *request.Devices
	return nil
}
