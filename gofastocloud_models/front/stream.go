package front

import (
	"gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type IStreamFront front.IStreamFront

type ProxyStreamFront front.ProxyStreamFront

func (s *ProxyStreamFront) ToPlayer(pid *string) player.ChannelInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, pid)
	urls := make([]player.PlayingUrl, 0)
	for _, outputUrl := range s.Output {
		play := player.MakePlayingUrlFromOutputUri(outputUrl)
		urls = append(urls, play)
	}
	epg := player.EpgInfo{
		ID:          s.TVGID,
		DisplayName: s.Name,
		Icon:        s.TVGLogo,
		Urls:        urls,
		Description: s.Description,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, EpgInfo: epg}
	return channel
}

type HardwareStreamFront front.HardwareStreamFront

func (s *HardwareStreamFront) ToPlayer(pid *string) player.ChannelInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, pid)
	urls := make([]player.PlayingUrl, 0)
	for _, outputUrl := range s.Output {
		play := player.MakePlayingUrlFromOutputUri(outputUrl)
		urls = append(urls, play)
	}
	epg := player.EpgInfo{
		ID:          s.TVGID,
		DisplayName: s.Name,
		Icon:        s.TVGLogo,
		Urls:        urls,
		Description: s.Description,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, EpgInfo: epg}
	return channel
}

type CatchupStreamFront front.CatchupStreamFront

func (s *CatchupStreamFront) ToPlayer(pid *string) player.CatchupInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, pid)
	urls := make([]player.PlayingUrl, 0)
	for _, outputUrl := range s.Output {
		play := player.MakePlayingUrlFromOutputUri(outputUrl)
		urls = append(urls, play)
	}
	epg := player.EpgInfo{
		ID:          s.TVGID,
		DisplayName: s.Name,
		Icon:        s.TVGLogo,
		Urls:        urls,
		Description: s.Description,
	}
	channel := player.ChannelInfo{StreamBaseInfo: *base, EpgInfo: epg}
	catchup := player.CatchupInfo{ChannelInfo: channel, StartRecord: s.StartRecord, StopRecord: s.StopRecord}
	return catchup
}

type VodProxyStreamFront front.VodProxyStreamFront

func (s *VodProxyStreamFront) ToPlayer(pid *string) player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, pid)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

type VodRelayStreamFront front.VodRelayStreamFront

func (s *VodRelayStreamFront) ToPlayer(pid *string) player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, pid)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

type VodEncodeStreamFront front.VodEncodeStreamFront

func (s *VodEncodeStreamFront) ToPlayer(pid *string) player.VodInfo {
	base := MakeStreamBaseInfo(&s.IStreamFront, pid)
	movie := MakeMovieInfo(&s.VodBasedStreamFront, &s.IStreamFront)
	vod := player.VodInfo{StreamBaseInfo: *base, VOD: *movie}
	return vod
}

func MakeStreamBaseInfo(istream *front.IStreamFront, pid *string) *player.StreamBaseInfo {
	if len(istream.Parts) == 0 {
		istream.Parts = make([]string, 0)
	}

	if len(istream.Subtitles) == 0 {
		istream.Subtitles = make([]front.Subtitle, 0)
	}

	var id string
	if istream.ID != nil {
		id = *istream.ID
	} else {
		id = ""
	}

	var createDate gofastogt.UtcTimeMsec
	if istream.CreatedDate != nil {
		createDate = *istream.CreatedDate
	} else {
		createDate = 0
	}

	return &player.StreamBaseInfo{
		ID:          id,
		Groups:      istream.Groups,
		IARC:        istream.IARC,
		Parts:       istream.Parts,
		Meta:        istream.Meta,
		CreatedDate: createDate,
		Substitles:  istream.Subtitles,

		PackageID: pid,
		Price:     istream.Price,

		//
		Favorite:         false,
		Recent:           0,
		InterruptionTime: 0,
	}
}

func MakeMovieInfo(baseVOD *front.VodBasedStreamFront, istream *front.IStreamFront) *player.MovieInfo {
	urls := make([]player.PlayingUrl, 0)
	for _, outputUrl := range istream.Output {
		play := player.MakePlayingUrlFromOutputUri(outputUrl)
		urls = append(urls, play)
	}
	if len(baseVOD.Directors) == 0 {
		baseVOD.Directors = make([]string, 0)
	}
	if len(baseVOD.Cast) == 0 {
		baseVOD.Cast = make([]string, 0)
	}

	if len(baseVOD.Production) == 0 {
		baseVOD.Production = make([]string, 0)
	}

	if len(baseVOD.Genres) == 0 {
		baseVOD.Genres = make([]string, 0)
	}

	if len(istream.Subtitles) == 0 {
		istream.Subtitles = make([]front.Subtitle, 0)
	}
	return &player.MovieInfo{
		DisplayName:   istream.Name,
		Description:   istream.Description,
		PreviewIcon:   istream.TVGLogo,
		Urls:          urls,
		BackgroundURL: baseVOD.BackgroundURL,
		TrailerUrl:    baseVOD.TrailerURL,
		UserScore:     baseVOD.UserScore,
		PrimeDate:     baseVOD.PrimeDate,
		Country:       baseVOD.Country,
		Directors:     baseVOD.Directors,
		Genres:        baseVOD.Genres,
		Production:    baseVOD.Production,
		Cast:          baseVOD.Cast,
		Duration:      baseVOD.Duration,
	}
}
