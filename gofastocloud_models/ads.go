package gofastocloud_models

import (
	"encoding/json"
	"errors"
)

type GoogleAdMob struct {
	AndroidAdUnit *AdmobUnit `yaml:"android,omitempty" json:"android,omitempty"`
	IOSAdUnit     *AdmobUnit `yaml:"ios,omitempty"     json:"ios,omitempty"`
}

func (a *GoogleAdMob) UnmarshalJSON(data []byte) error {
	request := struct {
		AndroidAdUnit *AdmobUnit `yaml:"android" json:"android"`
		IOSAdUnit     *AdmobUnit `yaml:"ios" json:"ios"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.AndroidAdUnit != nil {
		a.AndroidAdUnit = request.AndroidAdUnit
	}
	if request.IOSAdUnit != nil {
		a.IOSAdUnit = request.IOSAdUnit
	}

	return nil
}

type AdmobUnit struct {
	AppID        string  `yaml:"app_id"                 json:"app_id"`
	Banner       *string `yaml:"banner,omitempty"       json:"banner,omitempty"`
	Interstitial *string `yaml:"interstitial,omitempty" json:"interstitial,omitempty"`
	Rewarded     *string `yaml:"rewarded,omitempty"     json:"rewarded,omitempty"`
}

func (u *AdmobUnit) UnmarshalJSON(data []byte) error {
	request := struct {
		AppID        *string `json:"app_id,omitempty"`
		Banner       *string `json:"banner,omitempty"`
		Interstitial *string `json:"interstitial,omitempty"`
		Rewarded     *string `json:"rewarded,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.AppID != nil {
		u.AppID = *request.AppID
	}
	if request.Banner != nil {
		u.Banner = request.Banner
	}
	if request.Interstitial != nil {
		u.Interstitial = request.Interstitial
	}
	if request.Rewarded != nil {
		u.Rewarded = request.Rewarded
	}

	return nil
}

type PromotionType int

func (p PromotionType) IsValid() bool {
	return p == TEXT || p == LINK || p == IMAGE || p == VIDEO
}

const (
	TEXT PromotionType = iota
	LINK
	IMAGE
	VIDEO
)

type Promotion struct {
	PromotionType `yaml:"type" json:"type"`
	Url           string `yaml:"url" json:"url"`
}

func (p *Promotion) UnmarshalJSON(data []byte) error {
	request := struct {
		*PromotionType `yaml:"type" json:"type"`
		Url            *string `yaml:"url" json:"url"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.PromotionType == nil {
		return errors.New("type field is required")
	}
	if request.PromotionType != nil {
		if !request.PromotionType.IsValid() {
			return errors.New("invalid input type")
		}
		p.PromotionType = *request.PromotionType

		if request.Url == nil {
			return errors.New("invalid input Url")
		}
		p.Url = *request.Url
	}

	return nil
}

type Vast struct {
	Url string `yaml:"url" json:"url"`
}

func (v *Vast) UnmarshalJSON(data []byte) error {
	request := struct {
		Url *string `json:"url,omitempty"`
	}{}
	err := json.Unmarshal(data, &request)
	if err != nil {
		return err
	}
	if request.Url == nil {
		return errors.New("invalid vast url")
	}

	v.Url = *request.Url
	return nil
}
