package gofastocloud_models

import "go.mongodb.org/mongo-driver/bson/primitive"

func NewRawUniqueID() primitive.ObjectID {
	oid := primitive.NewObjectID()
	return oid
}

func NewUniqueID() string {
	oid := NewRawUniqueID()
	return oid.Hex()
}
