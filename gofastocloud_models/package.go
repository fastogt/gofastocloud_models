package gofastocloud_models

import (
	"time"

	mfront "gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front/player"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DBPackageFields struct {
	Name              string               `bson:"name"`
	Host              mfront.WSServerFront `bson:"host"`
	Local             bool                 `bson:"local"`
	Price             *mfront.PricePack    `bson:"price,omitempty"`
	Visible           bool                 `bson:"visible"`
	AvailableOnSignup bool                 `bson:"available_on_signup"`
	Description       string               `bson:"description"`
	BackgroundURL     string               `bson:"background_url"`
	Location          *gofastogt.Location  `bson:"location,omitempty"`
	// filled by server
	CreatedDate *time.Time `bson:"created_date,omitempty"`
	PID         *string    `bson:"pid,omitempty"`
}

func (ser *DBPackageFields) IsPaid() bool {
	return ser.Price != nil
}

type DBPackage struct {
	ID              primitive.ObjectID `bson:"_id"`
	DBPackageFields `bson:",inline"`
}

func (s *DBPackage) ToFront(version *string) *front.PackageFront {
	hexed := s.ID.Hex()
	var created gofastogt.UtcTimeMsec
	if s.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*s.CreatedDate)
	}
	play := player.PackagePublic{ID: &hexed,
		Name:          s.Name,
		Price:         s.Price,
		Description:   s.Description,
		BackgroundURL: s.BackgroundURL}

	pub := front.PackagePublicFront{
		PackagePublic: play,
		Location:      s.Location,
		CreatedDate:   &created,
		Version:       version,
	}

	return &front.PackageFront{
		PackagePublicFront: pub,
		Host:               s.Host,
		Local:              s.Local,
		Visible:            s.Visible,
		AvailableOnSignup:  s.AvailableOnSignup,
	}
}

func MakePackageFromFront(s *front.PackageFront) *DBPackageFields {
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	} else {
		created = time.Now()
	}

	result := DBPackageFields{
		Name:              s.Name,
		Host:              s.Host,
		Local:             s.Local,
		Price:             s.Price,
		Description:       s.Description,
		BackgroundURL:     s.BackgroundURL,
		Visible:           s.Visible,
		AvailableOnSignup: s.AvailableOnSignup,
		Location:          s.Location,
		CreatedDate:       &created,
	}
	return &result
}
