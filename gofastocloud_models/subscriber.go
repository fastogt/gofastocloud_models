package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DBSubscriberFields struct {
	Email     string                 `bson:"email"`
	FirstName string                 `bson:"first_name"`
	LastName  string                 `bson:"last_name"`
	Password  string                 `bson:"password"`
	ExpDate   time.Time              `bson:"exp_date"`
	Status    front.StatusSubscriber `bson:"status"`
	Country   string                 `bson:"country"`
	Language  string                 `bson:"language"`
	Servers   []primitive.ObjectID   `bson:"servers"`
	Phone     *string                `bson:"phone,omitempty"`
	Details   *string                `bson:"details,omitempty"`

	MaxDeviceCount uint64 `bson:"max_devices_count"`

	// filled by server
	SID              string               `bson:"sid"` // unique code for login
	IntegrationLinks []IntegrationLink    `bson:"ilinks"`
	Subscriptions    []primitive.ObjectID `bson:"subscriptions"`
	Devices          []DBDevice           `bson:"devices"`
	Playlists        []UserPlaylist       `bson:"playlists" `
	CreatedDate      *time.Time           `bson:"created_date,omitempty"`

	//UserFavoriteContent
	Streams  []UserStream `bson:"streams"`
	Vods     []UserStream `bson:"vods"`
	Episodes []UserStream `bson:"episodes"`
	Serials  []UserSerial `bson:"serials"`
}

type DBSubscriber struct {
	ID                 primitive.ObjectID `bson:"_id"`
	DBSubscriberFields `bson:",inline"`
}

type UserStream struct {
	ID               primitive.ObjectID     `bson:"id"`
	PID              primitive.ObjectID     `bson:"pid"`
	Favorite         bool                   `bson:"favorite"`
	Recent           gofastogt.UtcTimeMsec  `bson:"recent"`
	InterruptionTime gofastogt.DurationMsec `bson:"interruption_time"`
}

func MakeUserStream(pid primitive.ObjectID, sid primitive.ObjectID) *UserStream {
	return &UserStream{
		ID:               sid,
		PID:              pid,
		Favorite:         false,
		Recent:           0,
		InterruptionTime: 0,
	}
}

type UserSerial struct {
	ID       primitive.ObjectID    `bson:"id"`
	PID      primitive.ObjectID    `bson:"pid"`
	Favorite bool                  `bson:"favorite"`
	Recent   gofastogt.UtcTimeMsec `bson:"recent"`
}

func MakeUserSerial(pid primitive.ObjectID, sid primitive.ObjectID) *UserSerial {
	return &UserSerial{
		ID:       sid,
		PID:      pid,
		Favorite: false,
		Recent:   0,
	}
}

type UserPlaylist struct {
	IDPlaylist primitive.ObjectID `bson:"id"`
	Name       string             `bson:"name"`
	Url        string             `bson:"url"`
	Selected   bool               `bson:"select"`
}

func (playlist *UserPlaylist) ToFront() *front.UserPlaylistFront {
	hexed := playlist.IDPlaylist.Hex()
	return &front.UserPlaylistFront{
		IDPlaylist: &hexed,
		Name:       playlist.Name,
		Url:        playlist.Url,
		Selected:   playlist.Selected,
	}
}

func MakeUserPlaylistFromFront(p *front.UserPlaylistFront) (*UserPlaylist, error) {
	var id primitive.ObjectID
	var err error
	if p.IDPlaylist != nil {
		id, err = primitive.ObjectIDFromHex(*p.IDPlaylist)
		if err != nil {
			return nil, err
		}
	} else {
		id = NewRawUniqueID()
	}
	return &UserPlaylist{
		IDPlaylist: id,
		Name:       p.Name,
		Url:        p.Url,
		Selected:   p.Selected,
	}, nil
}

func (sub *DBSubscriber) ToFront(owner *string) *front.SubscriberFront {
	servers := make([]string, 0, len(sub.Servers))
	for _, s := range sub.Servers {
		servers = append(servers, primitive.ObjectID.Hex(s))
	}
	devices := make([]string, 0, len(sub.Devices))
	for _, d := range sub.Devices {
		devices = append(devices, primitive.ObjectID.Hex(d.IDDevice))
	}
	var created *gofastogt.UtcTimeMsec
	if sub.CreatedDate != nil {
		tmp := gofastogt.Time2UtcTimeMsec(*sub.CreatedDate)
		created = &tmp
	}
	hexed := sub.ID.Hex()
	up := front.SubscriberSignUpFront{
		Email:     sub.Email,
		FirstName: sub.FirstName,
		LastName:  sub.LastName,
		Password:  sub.Password,
		Country:   sub.Country,
		Language:  sub.Language,
		Phone:     sub.Phone,
		Details:   sub.Details,
	}
	add := front.SubscriberAddFront{
		SubscriberSignUpFront: up,
		CreatedDate:           created,
		ExpDate:               gofastogt.Time2UtcTimeMsec(sub.ExpDate),
		Status:                sub.Status,
		MaxDeviceCount:        sub.MaxDeviceCount,
		Servers:               servers,
		Owner:                 owner,
	}
	return &front.SubscriberFront{
		SubscriberAddFront: add,
		ID:                 &hexed,
		Devices:            devices,
		SID:                &sub.SID,
	}
}

func MakeSubscriberFromAddFront(s *front.SubscriberAddFront) (*DBSubscriberFields, error) {
	subServers := make([]primitive.ObjectID, 0, len(s.Servers))
	for _, s := range s.Servers {
		serverID, err := primitive.ObjectIDFromHex(s)
		if err != nil {
			return nil, err
		}
		subServers = append(subServers, serverID)
	}
	var created *time.Time
	if s.CreatedDate != nil {
		tmp := gofastogt.UtcTime2Time(*s.CreatedDate)
		created = &tmp
	}

	subscriber := DBSubscriberFields{
		Email:          s.Email,
		FirstName:      s.FirstName,
		LastName:       s.LastName,
		CreatedDate:    created,
		ExpDate:        gofastogt.UtcTime2Time(s.ExpDate),
		MaxDeviceCount: s.MaxDeviceCount,
		Servers:        subServers,
		Country:        s.Country,
		Language:       s.Language,
		Status:         s.Status,
		Password:       s.Password,
		Phone:          s.Phone,
		Details:        s.Details,
		Streams:        make([]UserStream, 0),
		Vods:           make([]UserStream, 0),
		Episodes:       make([]UserStream, 0),
		Serials:        make([]UserSerial, 0),
	}
	return &subscriber, nil
}

func (s *DBSubscriberFields) ContainsSubscription(sid primitive.ObjectID) bool {
	for _, sub := range s.Subscriptions {
		if sub == sid {
			return true
		}
	}
	return false
}

func (s *DBSubscriberFields) ContainsUserStream(pid primitive.ObjectID, sid primitive.ObjectID) bool {
	for _, uStream := range s.Streams {
		if uStream.ID == sid && uStream.PID == pid {
			return true
		}
	}
	return false
}

func (s *DBSubscriberFields) ContainsUserVods(pid primitive.ObjectID, sid primitive.ObjectID) bool {
	for _, uVod := range s.Vods {
		if uVod.ID == sid && uVod.PID == pid {
			return true
		}
	}
	return false
}

func (s *DBSubscriberFields) ContainsUserEpisodes(pid primitive.ObjectID, sid primitive.ObjectID) bool {
	for _, uVod := range s.Episodes {
		if uVod.ID == sid && uVod.PID == pid {
			return true
		}
	}
	return false
}

func (s *DBSubscriberFields) ContainsUserSerial(pid primitive.ObjectID, sid primitive.ObjectID) bool {
	for _, uSerial := range s.Serials {
		if uSerial.ID == sid && uSerial.PID == pid {
			return true
		}
	}
	return false
}

func (s *DBSubscriberFields) ContainsPackage(pid primitive.ObjectID) bool {
	for _, pack := range s.Servers {
		if pack == pid {
			return true
		}
	}
	return false
}

func (s *DBSubscriberFields) GetDevice(id primitive.ObjectID) *DBDevice {
	for _, device := range s.Devices {
		if device.IDDevice == id {
			return &device
		}
	}
	return nil
}

/*func (s *SubscriberFields) GetLastActiveTime() *time.Time {
	var lastActive *time.Time
	for _, device := range s.Devices {
		if lastActive == nil {
			*lastActive = *device.LastActiveTime
			continue
		}

		if device.LastActiveTime != nil && (*device.LastActiveTime).Sub(*lastActive) > 0 {
			*lastActive = *device.LastActiveTime
		}
	}
	return lastActive
}*/
