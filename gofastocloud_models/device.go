package gofastocloud_models

import (
	"time"

	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DBDeviceLoginInfo struct {
	IP        string    `bson:"ip"`
	Timestamp time.Time `bson:"timestamp"`
	Location  string    `bson:"loc"`
	City      string    `bson:"city"`
	Country   string    `bson:"country"`
}

func (device *DBDeviceLoginInfo) ToFront() *front.DeviceLoginInfoFront {
	dev := front.DeviceLoginInfoFront{IP: device.IP,
		Timestamp: gofastogt.Time2UtcTimeMsec(device.Timestamp),
		Location:  device.Location,
		City:      device.City,
		Country:   device.Country}
	return &dev
}

type DBDevice struct {
	IDDevice       primitive.ObjectID      `bson:"id"`
	Name           string                  `bson:"name"`
	Status         front.DeviceStatus      `bson:"status"`
	ActiveDuration *gofastogt.DurationMsec `bson:"active_duration,omitempty"`
	Logins         []DBDeviceLoginInfo     `bson:"logins,omitempty"`
	OS             *front.DeviceOS         `bson:"os,omitempty"`
	Project        *front.DeviceProject    `bson:"project,omitempty"`
	CPU            *front.CPUBrand         `bson:"cpu_brand,omitempty"`
	LastActiveTime *time.Time              `bson:"last_active,omitempty"`

	CreatedDate *time.Time `bson:"created_date,omitempty"`
}

func (device *DBDevice) ActivationPlace() *DBDeviceLoginInfo {
	logins := len(device.Logins)
	if logins > 0 {
		return &device.Logins[0]
	}
	return nil
}

func (device *DBDevice) LastLocation() *DBDeviceLoginInfo {
	logins := len(device.Logins)
	if logins > 0 {
		return &device.Logins[logins-1]
	}
	return nil
}

func NewDevice() *DBDevice {
	current := time.Now()
	return &DBDevice{
		IDDevice:    NewRawUniqueID(),
		Name:        "Device",
		CreatedDate: &current,
		Status:      front.NOT_ACTIVE_DEVICE,
	}
}

func (device *DBDevice) ToFront() *front.DeviceLoginFront {
	hexed := device.IDDevice.Hex()
	var created, lastActive gofastogt.UtcTimeMsec
	if device.CreatedDate != nil {
		created = gofastogt.Time2UtcTimeMsec(*device.CreatedDate)
	}

	if device.LastActiveTime != nil {
		lastActive = gofastogt.Time2UtcTimeMsec(*device.LastActiveTime)
	}

	logins := make([]front.DeviceLoginInfoFront, 0, len(device.Logins))
	for _, info := range device.Logins {
		dev := front.DeviceLoginInfoFront{Timestamp: gofastogt.Time2UtcTimeMsec(info.Timestamp),
			IP: info.IP, Location: info.Location, City: info.City, Country: info.Country}
		logins = append(logins, dev)
	}

	return &front.DeviceLoginFront{
		OS:             device.OS,
		Project:        device.Project,
		CPU:            device.CPU,
		ActiveDuration: device.ActiveDuration,
		Logins:         logins,
		LastActiveTime: &lastActive,
		DevicesFront: front.DevicesFront{
			IDDevice:    &hexed,
			Name:        device.Name,
			Status:      device.Status,
			CreatedDate: &created,
		},
	}
}

func MakeDeviceFromFront(s *front.DevicesFront) *DBDevice {
	var did primitive.ObjectID
	if s.IDDevice != nil {
		did, _ = primitive.ObjectIDFromHex(*s.IDDevice)
	}
	var created time.Time
	if s.CreatedDate != nil {
		created = gofastogt.UtcTime2Time(*s.CreatedDate)
	}
	return &DBDevice{
		Name:        s.Name,
		CreatedDate: &created,
		IDDevice:    did,
		Status:      s.Status,
	}
}

func (device *DBDevice) ToDeviceStatFront(sid primitive.ObjectID) *front.DeviceStatFront {
	logins := len(device.Logins)
	activate := device.ActivationPlace()
	var activateLocation *front.DeviceLoginInfoFront
	if activate != nil {
		activateLocation = activate.ToFront()
	}

	last := device.LastLocation()
	var lastLocation *front.DeviceLoginInfoFront
	if last != nil {
		lastLocation = last.ToFront()
	}

	var os gofastogt.PlatformType
	os = gofastogt.UNKNOWN
	if device.OS != nil {
		os = device.OS.Name
	}

	ud := front.SubscriberDevice{
		IDSubscriber: sid.Hex(),
		DeviceID:     device.IDDevice.Hex(),
		DeviceOS:     os,
		DeviceName:   device.Name,
	}

	return &front.DeviceStatFront{
		SubscriberDevice: ud,
		ActiveDuration:   device.ActiveDuration,
		LoginsCount:      int64(logins),
		ActivationPlace:  activateLocation,
		LastPlace:        lastLocation,
	}
}
