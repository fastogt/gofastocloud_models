package gofastocloud_models

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type DBSubscriptionFields struct {
	PackageID primitive.ObjectID `bson:"package"`
	Payment   string             `bson:"payment"`
}

type DBSubscription struct {
	ID                   primitive.ObjectID `bson:"_id"`
	DBSubscriptionFields `bson:",inline"`
}

func NewSubscription(id *primitive.ObjectID, pid string) *DBSubscriptionFields {
	return &DBSubscriptionFields{
		PackageID: *id,
		Payment:   pid,
	}
}
