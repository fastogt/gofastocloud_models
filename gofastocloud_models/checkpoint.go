package gofastocloud_models

import (
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
	"gitlab.com/fastogt/gofastogt/gofastogt"
)

type DBCheckPoint struct {
	Route             string `bson:"route"`
	DBDeviceLoginInfo `bson:",inline"`
}

func (check *DBCheckPoint) ToFront() *front.CheckPointFront {
	log := check.DBDeviceLoginInfo.ToFront()
	return &front.CheckPointFront{
		Route:                check.Route,
		DeviceLoginInfoFront: *log,
	}
}

func MakeCheckPointFromFront(check *front.CheckPointFront) *DBCheckPoint {
	return &DBCheckPoint{
		Route: check.Route,
		DBDeviceLoginInfo: DBDeviceLoginInfo{
			IP:        check.IP,
			Timestamp: gofastogt.UtcTime2Time(check.Timestamp),
			Location:  check.Location,
			City:      check.City,
			Country:   check.Country,
		},
	}
}
