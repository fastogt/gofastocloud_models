package unittests

import (
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models"
)

func TestShortM3uParse(t *testing.T) {
	data, err := os.ReadFile("short.m3u")
	assert.NoError(t, err)

	pack, err := gofastocloud_models.ParsePlaylistToPackage(string(data))
	assert.NoError(t, err)
	assert.Equal(t, len(pack.Streams), 2)
	assert.Equal(t, len(pack.Vods), 3)
	assert.Equal(t, len(pack.Episodes), 5)
	assert.Equal(t, len(pack.Seasons), 4)
	assert.Equal(t, len(pack.Serials), 2)

	assert.Equal(t, pack.Episodes[1].VOD.Urls[0].Url, "http://00322.xyz:8000/series/EcuaPlay@VodS22/3Il7@32M2n1IlbIl/161781.mkv")
	assert.Equal(t, pack.Episodes[1].VOD.DisplayName, "La playlist S02 E01")

	temp := strings.Split(pack.Vods[2].VOD.Urls[0].Url, ".")
	assert.Equal(t, temp[len(temp)-1], "avi")
}

func TestLongM3uParse(t *testing.T) {
	data, err := os.ReadFile("long.m3u")
	assert.NoError(t, err)

	pack, err := gofastocloud_models.ParsePlaylistToPackage(string(data))
	assert.NoError(t, err)

	assert.Equal(t, len(pack.Streams), 3)
	assert.Equal(t, len(pack.Vods), 2)
	assert.Equal(t, len(pack.Episodes), 5)
	assert.Equal(t, len(pack.Seasons), 4)
	assert.Equal(t, len(pack.Serials), 3)

	assert.Equal(t, pack.Episodes[2].VOD.Urls[0].Url, "http://00322.xyz:8000/series/EcuaPlay@VodS22/3Il7@32M2n1IlbIl/161797.mkv")
	assert.Equal(t, pack.Episodes[2].VOD.DisplayName, "Parentesco S01 Parentesco.S01.E06")

	temp := strings.Split(pack.Vods[1].VOD.Urls[0].Url, ".")
	assert.Equal(t, temp[len(temp)-1], "avi")
}

func TestSerM3uParse(t *testing.T) {
	data, err := os.ReadFile("ser.m3u")
	assert.NoError(t, err)

	pack, err := gofastocloud_models.ParsePlaylistToPackage(string(data))
	assert.NoError(t, err)

	assert.Equal(t, len(pack.Streams), 3)
	assert.Equal(t, len(pack.Vods), 5)
	assert.Equal(t, len(pack.Episodes), 57)
	assert.Equal(t, len(pack.Seasons), 3)
	assert.Equal(t, len(pack.Serials), 1)

	assert.Equal(t, pack.Episodes[0].VOD.Urls[0].Url, "https://some.com:443/play/d51oohTitipcFNbmr0uR9nmQNnOtgELDIbUfZnxvrt8J6zXIEqSifijMidK2ysdC#.mp4")
	assert.Equal(t, pack.Episodes[0].VOD.DisplayName, "Last Man Standing S01E01")

	temp := strings.Split(pack.Vods[1].VOD.Urls[0].Url, ".")
	assert.Equal(t, temp[len(temp)-1], "mp4")
}
