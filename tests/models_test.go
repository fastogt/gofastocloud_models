package unittests

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	mfront "gitlab.com/fastocloud/gofastocloud_media_models/gofastocloud_media_models/front"
	"gitlab.com/fastogt/gofastocloud_models/gofastocloud_models/front"
)

func TestProviderFrontUnmarshalJSON(t *testing.T) {
	testPrJSON := []byte(`{"amount": 10.0}`)
	var pr mfront.PricePack
	err := json.Unmarshal(testPrJSON, &pr)
	assert.Nil(t, err)
	assert.Equal(t, pr.Price, 10.0)

	testJSON := []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0
	  }`)
	var provider front.ProviderFront
	err = json.Unmarshal(testJSON, &provider)
	assert.Nil(t, err)

	// test pass by nil
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.NotNil(t, err)

	// test pass by empty
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "sdggsgs",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Nil(t, err)

	// test mail
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test first_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": ,
		"last_name": "last_name",
		"password": "efwef",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test last_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": ,
		"password": "",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test type
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "",
		"last_name": "last_name",
		"password": "",
		"created_date": 1651756917062,
		"type":,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test status
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "",
		"created_date": 1651756917062,
		"type":1,
		"status":,
		"country": "Belarus",
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test country
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": ,
		"language":"english",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test language
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": ,
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"",
		"credits":0,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

	// test credits
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"country": "Belarus",
		"language":"english",
		"credits":,
		"credits_remaining":0
	  }`)
	err = json.Unmarshal(testJSON, &provider)
	assert.Error(t, err)

}

func TestSubscriberFrontUnmarshalJSON(t *testing.T) {
	testJSON := []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["507f191e810c19729de860ea", "507f191e810c19729de860eb", "507f191e810c19729de860ec"],
		"devices": ["device1", "device2"]
	  }`)
	var subsriber front.SubscriberFront
	err := json.Unmarshal(testJSON, &subsriber)
	assert.Nil(t, err)

	// test pass by nil
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.NotNil(t, err)

	// test pass by empty
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"created_date": 1651756917062,
		"type":1,
		"status":1,
		"sid":"1651756917",
		"password":"",
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test sid by nil
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["507f191e810c19729de860ea", "507f191e810c19729de860eb", "507f191e810c19729de860ec"],
		"devices":["device1", "device2"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Nil(t, err)

	// test sid by empty
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"sid":"",
		"created_date": 1651756917062,
		"exp_date": 1651756917063,
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test mail
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test first_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": ,
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test last_name
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": ,
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test type
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test status
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test devices_count
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": ,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test max_devices_count
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": ,
		"country": "Belarus",
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test country
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": ,
		"language":"english",
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test language
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":,
		"servers": ["1", "2", "3"]
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	// test servers
	testJSON = []byte(`{
		"id":"9329j3r",
		"email":"1990@t.by",
		"first_name": "first_name",
		"last_name": "last_name",
		"password": "dfewwegweg",
		"created_date": 1651756917062,
		"sid":"1651756917",
		"type":1,
		"status":1,
		"devices_count": 2,
		"max_devices_count": 4,
		"country": "Belarus",
		"language":"english",
		"servers":
	  }`)
	err = json.Unmarshal(testJSON, &subsriber)
	assert.Error(t, err)

	testLiveJSON := []byte(`{"email":"topilski@mail.ru","first_name":"Alex","last_name":"Top","exp_date":1654497736943,"sid":"1651756917","status":1,"max_devices_count":10,"language":"en","country":"BY","servers":["627614bdf9e908ccf4e57179"],"devices_count":0,"password":"1023224","devices":["device1","device2"]}`)
	err = json.Unmarshal(testLiveJSON, &subsriber)
	assert.Nil(t, err)
}
