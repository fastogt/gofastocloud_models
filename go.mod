module gitlab.com/fastogt/gofastocloud_models

// gofastocloud
go 1.23.0

toolchain go1.24.1

require (
	github.com/stretchr/testify v1.10.0
	gitlab.com/fastocloud/gofastocloud v1.17.1
	gitlab.com/fastocloud/gofastocloud_media_models v1.1.3
	gitlab.com/fastogt/gofastogt v1.12.1
	go.mongodb.org/mongo-driver v1.17.3
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/websocket v1.5.3 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.31.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
